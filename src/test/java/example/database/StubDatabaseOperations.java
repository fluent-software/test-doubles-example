package example.database;

import example.salesanalysis.OrderSummary;

public class StubDatabaseOperations implements DatabaseOperations {
    private OrderSummary orderSummary;

    @Override
    public int insertOrder(OrderSummary order) {
        // we are a stub, not a mock, or fake implementation
        // so we don't really need to do anything here
        return 0;
    }

    @Override
    public OrderSummary findByID(int id) {
        return orderSummary;
    }

    public void setOrderSummary(OrderSummary orderSummary) {
        this.orderSummary = orderSummary;
    }
}
