package example.database;

import example.salesanalysis.OrderSummary;

public class SwitchableDatabaseOperations implements DatabaseOperations {

    private final DatabaseOperations defaultImplementation;
    private DatabaseOperations insertOrderImplementation;
    private DatabaseOperations findByIDImplementation;

    public SwitchableDatabaseOperations(DatabaseOperations defaultImplementation) {
        this.defaultImplementation = defaultImplementation;
    }

    @Override
    public int insertOrder(OrderSummary order) {
        if (insertOrderImplementation != null) {
            return insertOrderImplementation.insertOrder(order);
        }
        return defaultImplementation.insertOrder(order);
    }

    @Override
    public OrderSummary findByID(int id) {
        if (findByIDImplementation != null) {
            return findByIDImplementation.findByID(id);
        }
        return defaultImplementation.findByID(id);
    }

    public void setInsertOrderImplementation(DatabaseOperations insertOrderImplementation) {
        this.insertOrderImplementation = insertOrderImplementation;
    }

    public void setFindByIDImplementation(DatabaseOperations findByIDImplementation) {
        this.findByIDImplementation = findByIDImplementation;
    }
}
