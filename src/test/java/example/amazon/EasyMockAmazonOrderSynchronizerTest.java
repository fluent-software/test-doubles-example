package example.amazon;

import example.database.DatabaseOperations;
import example.database.StubDatabaseOperations;

import example.database.SwitchableDatabaseOperations;
import example.salesanalysis.OrderSummary;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.easymock.EasyMock.*;

public class EasyMockAmazonOrderSynchronizerTest {

    @Test
    public void testThatWhenRequestingAllOrderAreSynchronizedAllOrderDetailsAreRequestedFromAmazon() throws Exception {

        DatabaseOperations stubDatabaseOperations = new StubDatabaseOperations();

        AmazonClient mockAmazonClient = mock(AmazonClient.class);
        // our expectation
        expect(mockAmazonClient.getOrders()).andReturn(new ArrayList<Order>());
        replay(mockAmazonClient);

        // invoke the action that should cause the desired behaviour
        AmazonOrderSynchronizer synchronizer = new AmazonOrderSynchronizer(mockAmazonClient, stubDatabaseOperations);
        synchronizer.syncAllOrders();

        verify(mockAmazonClient);
    }

    @Test
    public void testThatWhenRequestingAllOrdersAreSynchronizedForEachAmazonOrderReceivedOrderSummaryDataIsCreatedInOurDatabase() throws Exception {

        List<Order> sourceOrders = Arrays.asList(new Order(), new Order());

        DatabaseOperations mockDatabaseOperations = mock(DatabaseOperations.class);
        AmazonClient stubAmazonClient = new StubAmazonClient(sourceOrders);

        // our expectation
        expect(mockDatabaseOperations.insertOrder(anyObject(OrderSummary.class))).andReturn(0).times(2);
        replay(mockDatabaseOperations);

        // invoke the action that should cause the desired behaviour
        AmazonOrderSynchronizer synchronizer = new AmazonOrderSynchronizer(stubAmazonClient, mockDatabaseOperations);
        synchronizer.syncAllOrders();

        verify(mockDatabaseOperations);
    }

    @Test
    public void testDemonstratingThatWeCouldUseDifferentStubsAndMocksForTheSameInterface() throws Exception {
        List<Order> sourceOrders = Arrays.asList(new Order(), new Order());

        // This is an incomplete test, but demonstrates that we could have a 'switchable' version of
        // a adependency that uses a mock implementation for one method, and stub implementation for others
        DatabaseOperations mockDatabaseOperations = mock(DatabaseOperations.class);
        DatabaseOperations stubDatabaseOperations = new StubDatabaseOperations();
        AmazonClient stubAmazonClient = new StubAmazonClient(sourceOrders);

        // Create a version of the database operation dependency that uses a stub by default for every method call
        // but a mock for the insertOrder call to allow verification
        SwitchableDatabaseOperations switchableDatabaseOperations = new SwitchableDatabaseOperations(stubDatabaseOperations);
        switchableDatabaseOperations.setInsertOrderImplementation(mockDatabaseOperations);

        // ... setup expectations ...
        replay(mockDatabaseOperations);

        // invoke the action that should cause the desired behaviour
        AmazonOrderSynchronizer synchronizer = new AmazonOrderSynchronizer(stubAmazonClient, mockDatabaseOperations);
        synchronizer.syncAllOrders();

        verify(mockDatabaseOperations);
    }
}
