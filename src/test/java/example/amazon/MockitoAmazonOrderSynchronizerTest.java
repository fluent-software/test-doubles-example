package example.amazon;

import example.database.DatabaseOperations;
import example.database.StubDatabaseOperations;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MockitoAmazonOrderSynchronizerTest {

    @Test
    public void testThatWhenRequestingAllOrderAreSynchronizedAllOrderDetailsAreRequestedFromAmazon() throws Exception {

        AmazonClient mockAmazonClient = mock(AmazonClient.class);
        // using explicit stub as with the easy mock example
        DatabaseOperations stubDatabaseOperations = new StubDatabaseOperations();

        AmazonOrderSynchronizer synchronizer = new AmazonOrderSynchronizer(mockAmazonClient, stubDatabaseOperations);
        synchronizer.syncAllOrders();

        verify(mockAmazonClient).getOrders();
    }

    @Test
    public void testThatWhenRequestingAllOrdersAreSynchronizedForEachAmazonOrderReceivedOrderSummaryDataIsCreatedInOurDatabase() throws Exception {

        List<Order> sourceOrders = Arrays.asList(new Order(), new Order());

        // using a mockito stub, but just be very careful if you do this not to accidentally start mocking
        // and verifying calls to this
        AmazonClient stubAmazonClient = mock(AmazonClient.class);

        DatabaseOperations mockDatabaseOperations = mock(DatabaseOperations.class);
        when(stubAmazonClient.getOrders()).thenReturn(sourceOrders);

        AmazonOrderSynchronizer synchronizer = new AmazonOrderSynchronizer(stubAmazonClient, mockDatabaseOperations);
        synchronizer.syncAllOrders();

        verify(mockDatabaseOperations, times(2)).insertOrder(any());
    }

}
