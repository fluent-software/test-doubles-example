package example.amazon;

import java.util.List;

public class StubAmazonClient implements AmazonClient {

    private List<Order> orders;

    public StubAmazonClient(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public List<Order> getOrders() {
        return orders;
    }
}
