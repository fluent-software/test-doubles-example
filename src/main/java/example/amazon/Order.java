package example.amazon;

/*
This class represents an order in Amazon format.
In reality, the AmazonClient and Order classes in this package would probably
be provided by a third party library or SDK.
*/
public class Order {
    /*
    There would be many more fields here, but the purpose of this
    is to demonstrate test double types rather than a fully fledged application
    */
    private int amazonID;
    private String orderTotal;

    public int getAmazonID() {
        return amazonID;
    }

    public void setAmazonID(int amazonID) {
        this.amazonID = amazonID;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }
}
