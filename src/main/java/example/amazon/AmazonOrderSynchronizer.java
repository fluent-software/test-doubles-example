package example.amazon;

import example.database.DatabaseOperations;
import example.salesanalysis.OrderSummary;

import java.util.List;

public class AmazonOrderSynchronizer {

    private final AmazonClient amazonClient;
    private final DatabaseOperations databaseOperations;

    public AmazonOrderSynchronizer(AmazonClient amazonClient, DatabaseOperations databaseOperations) {
        this.amazonClient = amazonClient;
        this.databaseOperations = databaseOperations;
    }

    public void syncAllOrders() throws Exception {
        List<Order> sourceOrders = amazonClient.getOrders();

        if (sourceOrders != null) {
            for (Order order : sourceOrders) {
                OrderSummary targetOrder = convertOrder(order);
                databaseOperations.insertOrder(targetOrder);
            }
        }
    }

    private OrderSummary convertOrder(Order source) {
        // This would do real field mapping and conversion in the real application
        // This is outside of the scope of our example tests, so we don't care what is
        // here for the purpose of this demonstration
        return new OrderSummary();
    }
}
