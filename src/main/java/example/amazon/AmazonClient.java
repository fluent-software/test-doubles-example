package example.amazon;

import java.util.List;

public interface AmazonClient {
    List<Order> getOrders();
}
