package example.database;

import example.salesanalysis.OrderSummary;

public interface DatabaseOperations {
    int insertOrder(OrderSummary order);
    OrderSummary findByID(int id);
}
