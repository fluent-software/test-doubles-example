package example.salesanalysis;

/**
 * This class represents our application specific view of an order no matter which
 * platform the order initially came from
 */
public class OrderSummary {
    int ID;
    String totalValue;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(String totalValue) {
        this.totalValue = totalValue;
    }
}
